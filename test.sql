/* Criar tabela */
CREATE TABLE usuarios(
  nome VARCHAR(30),
  email VARCHAR(50),
  idade INT
);

/* Inserir dados na tabela */
INSERT INTO usuarios(nome, email, idade) VALUES(
  'Leonardo Lima',
  'leonardo@mail.com',
  20
);

/* Listar todos os dados da tabela */
SELECT * FROM usuarios;

/* Listar apenas dados que satisfaçam determinada condição */
SELECT * FROM usuarios WHERE idade = 32;
SELECT * FROM usuarios WHERE nome = 'Leonardo Lima';
SELECT * FROM usuarios WHERE idade >= 18;

/* Deletar registros da tabela */
DELETE FROM usuarios WHERE nome = 'Napoleao';

/* Atualizar um ou mais campos na tabela */
UPDATE usuarios SET nome = "Nome de Teste" WHERE nome = "Joaquim Ferreira";
UPDATE usuarios SET nome = "Nome de Teste", idade = 45 WHERE nome = "João da Silva";
