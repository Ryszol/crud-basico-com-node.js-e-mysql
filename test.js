const Sequelize =require('sequelize')
const sequelize = new Sequelize('test', 'root', 'leo123',{
  host: 'localhost',
  dialect: 'mysql'
})

const Postagem = sequelize.define('postagens', {
  titulo: {
    type: Sequelize.STRING
  },
  conteudo: {
    type: Sequelize.TEXT
  }
})

const Usuario = sequelize.define('usuarios', {
  nome: {
    type: Sequelize.STRING
  },
  sobrenome: {
    type: Sequelize.STRING
  },
  idade: {
    type: Sequelize.INTEGER
  },
  email: {
    type: Sequelize.STRING
  }
})

//Postagem.sync({force: true})
//Usuario.sync({force: true})

Postagem.create({
  titulo: "Primeira postagem",
  conteudo: "Lorem ipsum dolor sit amet."
})

Usuario.create({
  nome: "Leonardo",
  sobrenome: "Lima",
  idade: 20,
  email: "leo@mail.com"
})

sequelize.authenticate()
  .then(function() {
    console.log("Conectado com sucesso!");
  })
  .catch(function(erro) {
    console.log(`Falha ao se conectar: ${erro}`);
  })
